<?php

namespace joliblog\Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class layoutController extends Controller
{
    public function indexAction()
    {
        return $this->render('joliblogBundle:layout:index.html.twig');
    }
}
